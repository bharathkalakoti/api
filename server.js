const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const ejs = require("ejs");
const app = express();
var nodemailer = require('nodemailer');
var persondetails = {name:'',email:'', amount:''}

require("dotenv").config();

const PORT = process.env.PORT || 8000;

const {initPayment, responsePayment} = require("./paytm/services/index");

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(__dirname + "/views"));
app.set("view engine", "ejs");

app.get("/default", (req, res) => {
  res.send("Hi"+ req.query.name);
});
app.get("/paywithpaytm", (req, res) => {
    persondetails.name=req.query.name;
    persondetails.email=req.query.email;
    persondetails.amount=req.query.amount;
    initPayment(persondetails.amount).then(
        success => {
            res.render("paytmRedirect.ejs", {
                resultData: success,
                paytmFinalUrl: process.env.PAYTM_FINAL_URL
            });
        },
        error => {
            res.send(error);
        }
    );
});

app.post("/paywithpaytmresponse", (req, res) => {
     
  var mailOptions = {
    from: 'kalakotibharath@gmail.com',
    to: persondetails.email,
    subject: 'Thanks for donating us',
    text:`Hey ${persondetails.name}, \nThanks for donating us \nAmount received ${req.body.TXNAMOUNT} \n\n\n\n Thanks,\n100 Smiles Charity`

  };
    responsePayment(req.body).then(
        success => {
            res.render("response.ejs", {resultData: "true", responseData: success});
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });
        },
        error => {
            res.send(error);
        }
    );
});

app.listen(PORT, () => {
    console.log("Running on " + PORT);
});


var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'kalakotibharath@gmail.com',
      pass: 'bharath@9977'
    }
  });
 

  app.post("/joinus", (req, res) => {
    let joindata = req.body;
    var usermailOptions = {
      from: "kalakotibharath@gmail.com",
      to: joindata.email,
      subject: 'Thanks for joining us',
      text:`Hey ${joindata.fullname}, \nThanks for joining us \n\n\n\n Thanks,\n100 Smiles Charity`
  
    };

    var adminmailOptions = {
      from: joindata.email,
      to: "kalakotibharath@gmail.com",
      subject: 'New Member Details',
      text:`Hi, \n\n Full Name : ${joindata.fullname} \n\n Phone Number : ${joindata.phone} \n\n Email : ${joindata.email} \n\n Message : ${joindata.message} Thanks for joining us \n\n\n\nThanks,\n100 Smiles Charity`
  
    };
    transporter.sendMail(usermailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        res.sendStatus(200);
      }
    });

    transporter.sendMail(adminmailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        res.sendStatus(200);
      }
    });
  });
  
  